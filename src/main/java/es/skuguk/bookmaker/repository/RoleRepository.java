package es.skuguk.bookmaker.repository;

import es.skuguk.bookmaker.entity.securety.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);
}
