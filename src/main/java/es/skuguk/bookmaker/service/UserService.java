package es.skuguk.bookmaker.service;

import es.skuguk.bookmaker.entity.User;
import es.skuguk.bookmaker.entity.securety.PasswordResetToken;
import es.skuguk.bookmaker.entity.securety.UserRole;

import java.util.Set;

public interface UserService {

    PasswordResetToken getPasswordResetToken(final String token);

    void createPasswordResetTokenForUser(final User user, final String token);

    User findByUsername(String username);

    User findByEmail(String email);

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save(User user);
}

