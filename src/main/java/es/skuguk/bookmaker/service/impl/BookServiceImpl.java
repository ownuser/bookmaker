package es.skuguk.bookmaker.service.impl;

import es.skuguk.bookmaker.entity.Book;
import es.skuguk.bookmaker.repository.BookRepository;
import es.skuguk.bookmaker.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> findAll() {
        return (List<Book>)bookRepository.findAll();
    }

    public Book findOne(Long id) {
        return bookRepository.findById(id).get();
    }
}
