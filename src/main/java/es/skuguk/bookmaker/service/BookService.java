package es.skuguk.bookmaker.service;

import es.skuguk.bookmaker.entity.Book;

import java.util.List;

public interface BookService {

    List<Book> findAll();

    Book findOne(Long id);
}
