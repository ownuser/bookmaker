package es.skuguk.bookmaker;

import es.skuguk.bookmaker.entity.User;
import es.skuguk.bookmaker.entity.securety.Role;
import es.skuguk.bookmaker.entity.securety.UserRole;
import es.skuguk.bookmaker.service.UserService;
import es.skuguk.bookmaker.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class BookmakerApplication implements CommandLineRunner {

    @Autowired
    UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(BookmakerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User();
        user1.setFirstname("Sergio");
        user1.setLastname("Kuguk");
        user1.setPassword(SecurityUtility.passwordEncoder().encode("p"));
        user1.setEmail("argentines99@gmail.com");

        Set<UserRole> userRole = new HashSet<>();
        Role role1 = new Role();
        role1.setRoleId(1);
        role1.setName("ROLE_USER");
        userRole.add(new UserRole(user1, role1));

        userService.createUser(user1, userRole);
    }
}
