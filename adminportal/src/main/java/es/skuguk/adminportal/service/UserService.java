package es.skuguk.adminportal.service;

import es.skuguk.adminportal.entity.User;
import es.skuguk.adminportal.entity.securety.UserRole;

import java.util.Set;

public interface UserService {

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save(User user);
}

