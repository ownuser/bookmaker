package es.skuguk.adminportal;


import es.skuguk.adminportal.entity.User;
import es.skuguk.adminportal.entity.securety.Role;
import es.skuguk.adminportal.entity.securety.UserRole;
import es.skuguk.adminportal.service.UserService;
import es.skuguk.adminportal.utility.SecurityUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class AdminportalApplication implements CommandLineRunner {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(AdminportalApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User();
        user1.setUsername("admin");
        user1.setPassword(SecurityUtility.passwordEncoder().encode("admin"));
        user1.setEmail("admin@gmail.com");

        Set<UserRole> userRole = new HashSet<>();
        Role role1 = new Role();
        role1.setRoleId(0);
        role1.setName("ROLE_ADMIN");
        userRole.add(new UserRole(user1, role1));

        userService.createUser(user1, userRole);
    }
}
