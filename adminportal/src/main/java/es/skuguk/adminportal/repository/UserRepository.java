package es.skuguk.adminportal.repository;

import es.skuguk.adminportal.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
}
