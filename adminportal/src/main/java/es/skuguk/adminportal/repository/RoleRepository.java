package es.skuguk.adminportal.repository;

import es.skuguk.adminportal.entity.securety.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);
}
