package es.skuguk.adminportal.repository;

import es.skuguk.adminportal.entity.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {


}
